<?php
/*
Plugin Name: Immortal
Description: Immortal
Version: 1.0.0
Plugin URI: https://www.fiverr.com/logics_buffer
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/

function ace_hide_prices_category_pages( $price, $product ) {
	if ( is_tax() && $product->has_child()) { // Change 'clothing' to your product category slug
		return 'PRICE DEPENDS ON OPTIONS'; // Return a empty string for no price display.
	}
	return $price;
}
add_filter( 'woocommerce_get_price_html', 'ace_hide_prices_category_pages', 10, 2 );

add_filter('woocommerce_available_payment_gateways','bbloomer_unset_gateway_by_category');
 
function bbloomer_unset_gateway_by_category($available_gateways){
global $woocommerce;
$category_slugs = array('quotation');
foreach ($woocommerce->cart->cart_contents as $key => $values ) {
$terms = get_the_terms( $values['product_id'], 'product_cat' );
if($terms){
foreach ($terms as $term) {        
if(in_array($term->slug, $category_slugs)){
    unset($available_gateways['bacs']);
	unset($available_gateways['cheque']);
	unset( $available_gateways['paypal'] );
	unset( $available_gateways['sagepaydirect'] );
            break;
        }
    break;
    }
}
}
    return $available_gateways;
}
add_action( 'init', 'immortal' );

function immortal() {

	add_shortcode( 'show_immortal_form', 'wp_immortal_form' );
	add_action( 'wp_enqueue_scripts', 'immortal_script' );
	add_action( 'wp_ajax_nopriv_post_love_set_product_canvas', 'post_love_set_product_canvas' );
	add_action( 'wp_ajax_post_love_set_product_canvas', 'post_love_set_product_canvas' );
}

 function immortal_script() {
		        
	
	wp_enqueue_script( 'rt_masking_script', plugins_url().'/immortal/js/masking.js',array(),time());
	wp_enqueue_script( 'jquery-ui', plugins_url().'/immortal/js/jquery-ui.js',array(),time());
	//wp_enqueue_script( 'rt_button_script', plugins_url().'/immortal/js/fa-multi-button.js',array(),time());
	wp_enqueue_style( 'rt_custom_fancy_style', plugins_url().'/immortal/css/custom_fancy.css',array(),time());
	wp_enqueue_style( 'wcg_public', plugins_url().'/immortal/css/wcg-public.css',array(),time());

	if( is_single('dota-2-mmr-boost')){
		wp_enqueue_script( 'rt_custom_fancy_script', plugins_url().'/immortal/js/custom_fancy1.js',array(),time());	
	//wp_enqueue_style( 'rt_fancy_bootstrap', plugins_url().'/immortal/css/bootstrap1.css',array(),time());
	}
} 
function post_love_set_product_canvas(){
			$rand = rand(1, 9000);
			// Insert the post into the database
			$can_height = $_REQUEST['mmr_points'];
			$can_width = $_REQUEST['can_width'];
			$size_unit = $_REQUEST['size_unit'];
			$banner_qty = $_REQUEST['banner_qty'];
			$banner_material = $_REQUEST['banner_material'];
			$banner_finishing = $_REQUEST['banner_finishing'];
			$banner_delivery = $_REQUEST['banner_delivery'];
			$per_banner_price = $_REQUEST['per_banner_price'];
			$height = $_REQUEST['cheight'];
			$width = $_REQUEST['cwidth'];
			$inch_height = $_REQUEST['inch_height'];
			$inch_width = $_REQUEST['inch_width'];
			$product_title ="<p>Material: ".ucfirst($banner_material).'</br>'.$can_width.' '.ucfirst($size_unit).' &#10005; '.$can_height.' '.ucfirst($size_unit).'</br>Finishing: '.ucfirst($banner_finishing).'</br>Delivery: '.ucfirst($banner_delivery).'</br>Quantity: '.ucfirst($banner_qty)."</p>";
			if($inch_height){
				$can_height = floor($can_height).' foot-'.$inch_height.' inch';
				
			}
			if($inch_width){
				$can_width = floor($can_width).' foot-'.$inch_width.' inch';
			}
			if($inch_width || $inch_height){
			$product_title ="<p>Material: ".ucfirst($banner_material).'</br>'.floor($can_width).' '.ucfirst($size_unit).' '.$inch_width.' inch'.' &#10005; '.floor($can_height).' '.ucfirst($size_unit).' '.$inch_height.' inch'.'</br>Finishing: '.ucfirst($banner_finishing).'</br>Deliverffy: '.ucfirst($banner_delivery).'</br>Quantity: '.ucfirst($banner_qty)."</p>";
			}
			$price = $_REQUEST['price'];
			$my_post = array(
			  'post_title'    => $product_title,
			  'post_content'  => 'test_post',
			  'post_type' => 'product',
			  'post_status'   => 'publish'
			);
			$product_id = wp_insert_post( $my_post );
			$canvas_size = '{\\&quot;stage_width\\&quot;:\\&quot;'.$width.'\\&quot;,\\&quot;stage_height\\&quot;:\\&quot;'.$height.'\\&quot;}';
			//$fancy_product_id = 1;
			//get_product_html( $product_id );
				global $wpdb;
				$table_name = $wpdb->prefix . 'fpd_products';
				$wpdb->insert( 
					$table_name, 
					array( 
						'ID' => $product_id, 
						'title' => $product_title,
						'options' => $canvas_size,
						'thumbnail' => 'http://designtool.efound.co.uk/wp-content/uploads/2016/11/default_image_01.png' 
					) 
				);
				$table_name = $wpdb->prefix . 'fpd_views';
				$wpdb->insert( 
					$table_name, 
					array( 
						'ID' => '', 
						'product_id' => $product_id,
						'title' => 'front',
						'thumbnail' => 'http://designtool.efound.co.uk/wp-content/uploads/2016/11/default_image_01.png',
						'elements' => '', 
						'view_order' => '0',
						'options' => '' 
					) 
				);
				update_post_meta($product_id, 'fpd_source_type', 'product'); 
				update_post_meta($product_id, 'fpd_products', $product_id);
				update_post_meta($product_id, '_price', $price);
				update_post_meta($product_id, '_regular_price', $price);
				update_post_meta($product_id, 'width_wdith', $can_width);
				update_post_meta($product_id, 'width_length',  $can_height);
				update_post_meta($product_id, 'width_unit',    $size_unit);			
				update_post_meta($product_id, 'width_qty', $banner_qty);
				update_post_meta($product_id, 'width_delivery', $banner_delivery);
				update_post_meta($product_id, 'width_finishing', $banner_finishing);
				update_post_meta($product_id, 'width_material', $banner_material);
				update_post_meta($product_id, 'per_banner_price', $per_banner_price);
				$url = get_permalink($product_id);
				print_r($url);
				die();
	
}
	
		
function wp_immortal_form() {
		$plugins_url =  plugins_url( 'images/icons/', __FILE__ );
			global $post;
			//$pagerr = get_page_by_title('DOTA 2 MMR Boost', OBJECT, 'product');
			
			$new_post_id = get_the_ID();
			?><pre><?php print_r('$new_post_id'); ?></pre><?php
			?><pre><?php print_r($new_post_id); ?></pre><?php
			if(isset($_POST['submit_prod'])){
			//$price_oneway = $_POST['pricetotal_oneway'];
			global $woocommerce;
			$mmr_points= $_POST['mmr_points'];
			$estimated_days= $_POST['estimated_days'];
			$my_mrr = $_POST['my_mrr'];
			$desirder_mrr= $_POST['desirder_mrr'];			
			$rush_my_order = $_POST['rush_my_order'];			 
			$i_want_stream = $_POST['i_want_stream'];			 
			$schedule_time = $_POST['schedule_time'];	
			$i_choose_heros = $_POST['i_choose_heros'];
			$game_type = $_POST['game_type'];			
			$address = $_POST['rt_addr'];			
			$comment = $_POST['rt_comment'];					
			/* $new_post = array(
			'post_title' => 'Custom Banner '.$rand,
			'post_content' => 'Lorem ipsum dolor sit amet...',
			'post_status' => 'publish',
			'post_type' => 'product'
			); */
			$skuu = "custom_dota_2";
			
			//$new_post_id = 1110;
			update_post_meta($new_post_id, '_sku', $skuu );
			update_post_meta( $new_post_id, '_price', $rt_price );
			update_post_meta( $new_post_id,'_regular_price', $rt_price );
				 			
				//set product values:
				update_option( 'wdyw_product',$new_post_id );
				update_post_meta( $new_post_id, '_stock_status', 'instock');
				update_post_meta( $new_post_id, '_weight', "0.06" );
				update_post_meta( $new_post_id, '_Sold_individually', "1" );
				update_post_meta( $new_post_id, '_stock', "1000" );
				update_post_meta( $new_post_id, '_visibility', 'visible' );
				update_post_meta($new_post_id, 'mmr_points', $mmr_points);
				update_post_meta($new_post_id, 'estimated_days', $estimated_days);
				update_post_meta($new_post_id, 'my_mrr',  $my_mrr);
				update_post_meta($new_post_id, 'desirder_mrr',$desirder_mrr);			
				update_post_meta($new_post_id, 'rush_my_order',$rush_my_order);			
				update_post_meta($new_post_id, 'i_want_stream',$i_want_stream);			
				update_post_meta($new_post_id, 'schedule_time',$schedule_time);			
				update_post_meta($new_post_id, 'i_choose_heros',$i_choose_heros);			
				update_post_meta($new_post_id, 'game_type',$game_type);					
				$woocommerce->cart->add_to_cart($new_post_id);
				$cart_url = site_url().'/cart/';
				wp_redirect( $cart_url );
				exit;
			}
			
ob_start();
	
			?>
<div class="custom_calculator wcg-slider-content">
		<div id="rt_waiting" style="display: none;"><img src="http://vinylbannersprinting.co.uk/wp-content/uploads/2017/01/ajax_loader.gif" style="width: 120px;"></div>
	<h3 class="rp_wcdpd_product_page_title">Dota 2 MMR Boost</h3>
	<div class="orderform" id="form1">

	
	<form role="form" action="" method="post" id="add_cart" enctype="multipart/form-data" method="post">						
	 <div class="form-inner-main">
				<div class="items_group clearfix">				    						
				<!-- <span id="price"/></span> -->
				<input type="hidden" id="eprice" />
				<input type="hidden" id="hprice" />						
				<input type="hidden" id="psqft" />						
				<input type="hidden" id="efinal" />
				<input type="hidden" id="qfinal" />
				<input type="hidden" id="sqft_total" />
				<input type="hidden" id="eyeletsT1" />
				<input type="hidden" id="hammingT1" />
				<input type="hidden" id="deliveryT1" />			

 <div class="double_sided images">											

<div class="wcg_input_row">
				
	<div class="wcg_field wcg_center">
		<button type="button" id="solo" class="tab-btn active" onclick="calculate_addition(this.id)" value="solo">SOLO</button>
		<button type="button" id="party" class="tab-btn" onclick="calculate_addition(this.id)" value="party">PARTY</button>
		<input class="hide" type="text" id="game_type" name="game_type" value="solo"> 
	</div>
	
</div>
			<div class="medals_icons_container images wcg_input_row">											
				<div class="wcg_field wcg_left" id="my_current_mmr">

					<img id="119_mmr" class="medal_icon" src="<?php echo $plugins_url.'119_mmr.jpg'; ?>">								
					<img id="239_mmr" class="medal_icon" src="<?php echo $plugins_url.'239_mmr.jpg'; ?>">								
					<img id="359_mmr" class="medal_icon" src="<?php echo $plugins_url.'359_mmr.jpg'; ?>">								
					<img id="479_mmr" class="medal_icon" src="<?php echo $plugins_url.'479_mmr.jpg'; ?>">	

					<img id="599_mmr" class="medal_icon" src="<?php echo $plugins_url.'599_mmr.jpg'; ?>">								
					<img id="719_mmr" class="medal_icon" src="<?php echo $plugins_url.'719_mmr.jpg'; ?>">								
					<img id="839_mmr" class="medal_icon" src="<?php echo $plugins_url.'839_mmr.jpg'; ?>">								
					<img id="959_mmr" class="medal_icon" src="<?php echo $plugins_url.'959_mmr.jpg'; ?>">
					
					<img id="1079_mmr" class="medal_icon" src="<?php echo $plugins_url.'1079_mmr.jpg'; ?>">								
					<img id="1199_mmr" class="medal_icon" src="<?php echo $plugins_url.'1199_mmr.jpg'; ?>">							
					<img id="1319_mmr" class="medal_icon" src="<?php echo $plugins_url.'1319_mmr.jpg'; ?>">								
					<img id="1439_mmr" class="medal_icon" src="<?php echo $plugins_url.'1439_mmr.jpg'; ?>">	
					
					<img id="1559_mmr" class="medal_icon" src="<?php echo $plugins_url.'1559_mmr.jpg'; ?>">								
					<img id="1679_mmr" class="medal_icon" src="<?php echo $plugins_url.'1679_mmr.jpg'; ?>">								
					<img id="1799_mmr" class="medal_icon" src="<?php echo $plugins_url.'1799_mmr.jpg'; ?>">								
					<img id="1919_mmr" class="medal_icon" src="<?php echo $plugins_url.'1919_mmr.jpg'; ?>">	
					
					<img id="2039_mmr" class="medal_icon" src="<?php echo $plugins_url.'2039_mmr.jpg'; ?>">							
					<img id="2159_mmr" class="medal_icon" src="<?php echo $plugins_url.'2159_mmr.jpg'; ?>">								
					<img id="2279_mmr" class="medal_icon" src="<?php echo $plugins_url.'2279_mmr.jpg'; ?>">								
					<img id="2399_mmr" class="medal_icon" src="<?php echo $plugins_url.'2399_mmr.jpg'; ?>">	
								
					<img id="2519_mmr" class="medal_icon" src="<?php echo $plugins_url.'2519_mmr.jpg'; ?>">								
					<img id="2639_mmr" class="medal_icon" src="<?php echo $plugins_url.'2639_mmr.jpg'; ?>">								
					<img id="2759_mmr" class="medal_icon" src="<?php echo $plugins_url.'2759_mmr.jpg'; ?>">								
					<img id="2879_mmr" class="medal_icon" src="<?php echo $plugins_url.'2879_mmr.jpg'; ?>">	

					<img id="2999_mmr" class="medal_icon" src="<?php echo $plugins_url.'2999_mmr.jpg'; ?>">								
					<img id="3119_mmr" class="medal_icon" src="<?php echo $plugins_url.'3119_mmr.jpg'; ?>">								
					<img id="3239_mmr" class="medal_icon" src="<?php echo $plugins_url.'3239_mmr.jpg'; ?>">								
					<img id="3359_mmr" class="medal_icon" src="<?php echo $plugins_url.'3359_mmr.jpg'; ?>">	
					
					<img id="3479_mmr" class="medal_icon" src="<?php echo $plugins_url.'3479_mmr.jpg'; ?>">								
					<img id="3599_mmr" class="medal_icon" src="<?php echo $plugins_url.'3599_mmr.jpg'; ?>">								
					<img id="3719_mmr" class="medal_icon" src="<?php echo $plugins_url.'3719_mmr.jpg'; ?>">								
					<img id="3839_mmr" class="medal_icon" src="<?php echo $plugins_url.'3839_mmr.jpg'; ?>">	
								
					<img id="3959_mmr" class="medal_icon" src="<?php echo $plugins_url.'3959_mmr.jpg'; ?>">								
					<img id="4079_mmr" class="medal_icon" src="<?php echo $plugins_url.'4079_mmr.jpg'; ?>">								
					<img id="4199_mmr" class="medal_icon" src="<?php echo $plugins_url.'4199_mmr.jpg'; ?>">								
					<img id="4319_mmr" class="medal_icon" src="<?php echo $plugins_url.'4319_mmr.png'; ?>">	
								
					<img id="4439_mmr" class="medal_icon" src="<?php echo $plugins_url.'4439_mmr.jpg'; ?>">								
					<img id="4559_mmr" class="medal_icon" src="<?php echo $plugins_url.'4559_mmr.jpg'; ?>">								
					<img id="4679_mmr" class="medal_icon" src="<?php echo $plugins_url.'4679_mmr.jpg'; ?>">								
					<img id="4799_mmr" class="medal_icon" src="<?php echo $plugins_url.'4799_mmr.jpg'; ?>">	
					
					<img id="4919_mmr" class="medal_icon" src="<?php echo $plugins_url.'4919_mmr.jpg'; ?>">								
					<img id="5039_mmr" class="medal_icon" src="<?php echo $plugins_url.'5039_mmr.jpg'; ?>">								
					<img id="5159_mmr" class="medal_icon" src="<?php echo $plugins_url.'5159_mmr.jpg'; ?>">								
					<img id="5279_mmr" class="medal_icon" src="<?php echo $plugins_url.'5279_mmr.jpg'; ?>">	
					
					<img id="5399_mmr" class="medal_icon" src="<?php echo $plugins_url.'5399_mmr.jpg'; ?>">								
					<img id="5519_mmr" class="medal_icon" src="<?php echo $plugins_url.'5519_mmr.jpg'; ?>">								
					<img id="5639_mmr" class="medal_icon" src="<?php echo $plugins_url.'5639_mmr.jpg'; ?>">								
					<img id="5759_mmr" class="medal_icon" src="<?php echo $plugins_url.'5759_mmr.jpg'; ?>">	
					<img id="5740_mmr" class="medal_icon" src="<?php echo $plugins_url.'5740_mmr.jpg'; ?>">					
					<img id="6200_mmr" class="medal_icon" src="<?php echo $plugins_url.'placed.jpg'; ?>">	

								
									
				</div>	

				<div class="wcg_field wcg_right" id="my_desired_mmr">

					<img id="119_mmr" class="medal_icon" src="<?php echo $plugins_url.'119_mmr.jpg'; ?>">								
					<img id="239_mmr" class="medal_icon" src="<?php echo $plugins_url.'239_mmr.jpg'; ?>">								
					<img id="359_mmr" class="medal_icon" src="<?php echo $plugins_url.'359_mmr.jpg'; ?>">								
					<img id="479_mmr" class="medal_icon" src="<?php echo $plugins_url.'479_mmr.jpg'; ?>">	

					<img id="599_mmr" class="medal_icon" src="<?php echo $plugins_url.'599_mmr.jpg'; ?>">								
					<img id="719_mmr" class="medal_icon" src="<?php echo $plugins_url.'719_mmr.jpg'; ?>">								
					<img id="839_mmr" class="medal_icon" src="<?php echo $plugins_url.'839_mmr.jpg'; ?>">								
					<img id="959_mmr" class="medal_icon" src="<?php echo $plugins_url.'959_mmr.jpg'; ?>">
					
					<img id="1079_mmr" class="medal_icon" src="<?php echo $plugins_url.'1079_mmr.jpg'; ?>">								
					<img id="1199_mmr" class="medal_icon" src="<?php echo $plugins_url.'1199_mmr.jpg'; ?>">							
					<img id="1319_mmr" class="medal_icon" src="<?php echo $plugins_url.'1319_mmr.jpg'; ?>">								
					<img id="1439_mmr" class="medal_icon" src="<?php echo $plugins_url.'1439_mmr.jpg'; ?>">	
					
					<img id="1559_mmr" class="medal_icon" src="<?php echo $plugins_url.'1559_mmr.jpg'; ?>">								
					<img id="1679_mmr" class="medal_icon" src="<?php echo $plugins_url.'1679_mmr.jpg'; ?>">								
					<img id="1799_mmr" class="medal_icon" src="<?php echo $plugins_url.'1799_mmr.jpg'; ?>">								
					<img id="1919_mmr" class="medal_icon" src="<?php echo $plugins_url.'1919_mmr.jpg'; ?>">	
					
					<img id="2039_mmr" class="medal_icon" src="<?php echo $plugins_url.'2039_mmr.jpg'; ?>">							
					<img id="2159_mmr" class="medal_icon" src="<?php echo $plugins_url.'2159_mmr.jpg'; ?>">								
					<img id="2279_mmr" class="medal_icon" src="<?php echo $plugins_url.'2279_mmr.jpg'; ?>">								
					<img id="2399_mmr" class="medal_icon" src="<?php echo $plugins_url.'2399_mmr.jpg'; ?>">	
								
					<img id="2519_mmr" class="medal_icon" src="<?php echo $plugins_url.'2519_mmr.jpg'; ?>">								
					<img id="2639_mmr" class="medal_icon" src="<?php echo $plugins_url.'2639_mmr.jpg'; ?>">								
					<img id="2759_mmr" class="medal_icon" src="<?php echo $plugins_url.'2759_mmr.jpg'; ?>">								
					<img id="2879_mmr" class="medal_icon" src="<?php echo $plugins_url.'2879_mmr.jpg'; ?>">	

					<img id="2999_mmr" class="medal_icon" src="<?php echo $plugins_url.'2999_mmr.jpg'; ?>">								
					<img id="3119_mmr" class="medal_icon" src="<?php echo $plugins_url.'3119_mmr.jpg'; ?>">								
					<img id="3239_mmr" class="medal_icon" src="<?php echo $plugins_url.'3239_mmr.jpg'; ?>">								
					<img id="3359_mmr" class="medal_icon" src="<?php echo $plugins_url.'3359_mmr.jpg'; ?>">	
					
					<img id="3479_mmr" class="medal_icon" src="<?php echo $plugins_url.'3479_mmr.jpg'; ?>">								
					<img id="3599_mmr" class="medal_icon" src="<?php echo $plugins_url.'3599_mmr.jpg'; ?>">								
					<img id="3719_mmr" class="medal_icon" src="<?php echo $plugins_url.'3719_mmr.jpg'; ?>">								
					<img id="3839_mmr" class="medal_icon" src="<?php echo $plugins_url.'3839_mmr.jpg'; ?>">	
								
					<img id="3959_mmr" class="medal_icon" src="<?php echo $plugins_url.'3959_mmr.jpg'; ?>">								
					<img id="4079_mmr" class="medal_icon" src="<?php echo $plugins_url.'4079_mmr.jpg'; ?>">								
					<img id="4199_mmr" class="medal_icon" src="<?php echo $plugins_url.'4199_mmr.jpg'; ?>">								
					<img id="4319_mmr" class="medal_icon" src="<?php echo $plugins_url.'4319_mmr.png'; ?>">	
								
					<img id="4439_mmr" class="medal_icon" src="<?php echo $plugins_url.'4439_mmr.jpg'; ?>">								
					<img id="4559_mmr" class="medal_icon" src="<?php echo $plugins_url.'4559_mmr.jpg'; ?>">								
					<img id="4679_mmr" class="medal_icon" src="<?php echo $plugins_url.'4679_mmr.jpg'; ?>">								
					<img id="4799_mmr" class="medal_icon" src="<?php echo $plugins_url.'4799_mmr.jpg'; ?>">	
					
					<img id="4919_mmr" class="medal_icon" src="<?php echo $plugins_url.'4919_mmr.jpg'; ?>">								
					<img id="5039_mmr" class="medal_icon" src="<?php echo $plugins_url.'5039_mmr.jpg'; ?>">								
					<img id="5159_mmr" class="medal_icon" src="<?php echo $plugins_url.'5159_mmr.jpg'; ?>">								
					<img id="5279_mmr" class="medal_icon" src="<?php echo $plugins_url.'5279_mmr.jpg'; ?>">	
					
					<img id="5399_mmr" class="medal_icon" src="<?php echo $plugins_url.'5399_mmr.jpg'; ?>">								
					<img id="5519_mmr" class="medal_icon" src="<?php echo $plugins_url.'5519_mmr.jpg'; ?>">								
					<img id="5639_mmr" class="medal_icon" src="<?php echo $plugins_url.'5639_mmr.jpg'; ?>">								
					<img id="5740_mmr" class="medal_icon" src="<?php echo $plugins_url.'5740_mmr.jpg'; ?>">	
					<img id="6200_mmr" class="medal_icon" src="<?php echo $plugins_url.'placed.jpg'; ?>">	
								
									
				</div>				
			</div> 
<div class="wcg_input_row">
  <div class="wcg_field wcg_left">
  	 <label for="my_mrr" style="font-size: 14px;line-height: 22px;color: #fff;">MY CURRENT MMR</label>
 	 <input type="text" id="my_mrr"  style="border:0; color:#f6931f; font-weight:bold;">
  </div>
  
  <div class="wcg_field wcg_right">
  	<label for="desirder_mrr" style="font-size: 14px;line-height: 22px;color: #fff;">DESIRED MMR</label>
  	<input type="text" id="desirder_mrr" style="border:0; color:#f6931f; font-weight:bold;">
  </div>
</div>
 
<div class="wcg_input_row" id="range_parent">
	<div id="slider-range"></div>

	<div id="hide_slider_row" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content ui-slider-pips">
    	   	
    	<span class="ui-slider-pip ui-slider-pip-label ui-slider-pip-1400" style="left: 1.3918%"><span class="ui-slider-line"></span>
    	<span class="ui-slider-label" data-value="1400">210</span></span>
    	<span class="ui-slider-pip ui-slider-pip-label ui-slider-pip-1400" style="left: 21.30%;"><span class="ui-slider-line"></span>
    	<span class="ui-slider-label" data-value="1400">1400</span></span>
    	<span class="ui-slider-pip ui-slider-pip-label ui-slider-pip-2800" style="left: 44%;"><span class="ui-slider-line"></span><span class="ui-slider-label" data-value="2800">2800</span></span>
    	<span class="ui-slider-pip ui-slider-pip-label ui-slider-pip-4200" style="left: 70.1754%"><span class="ui-slider-line"></span><span class="ui-slider-label" data-value="4200">4200</span></span>
    	<span class="ui-slider-pip ui-slider-pip-label ui-slider-pip-5600" style="left: 93%;"><span class="ui-slider-line"></span><span class="ui-slider-label" data-value="5775">5775</span></span>
    	<span class="ui-slider-pip ui-slider-pip-last ui-slider-pip-label ui-slider-pip-5985" style="left: 100%"><span class="ui-slider-line"></span><span class="ui-slider-label" data-value="6195">6195</span></span>
	</div>
</div>
<div class="wcg_input_row rush_order">
		<div class="wcg_field wcg_left">
			<button type="button" id="rush_my_order" name="rush_my_order" value="neutral" class="button_switch" onclick="calculate_addition(this.id)">RUSH MY ORDER</button>
		</div>
		<div class="wcg_field wcg_right">
			<button type="button" id="schedule_time" name="schedule_time" value="off" class="button_switch" onclick="calculate_addition(this.id)">SCHEDULE TIME</button>
		</div>
</div>
<div class="wcg_input_row">
		<div class="wcg_field wcg_left">
			<button type="button" id="i_want_stream" name="i_want_stream" value="off" class="button_switch" onclick="calculate_addition(this.id)">I WANT STREAM</button>
		</div>
		<div class="wcg_field wcg_right">
			<button type="button" id="i_choose_heros" name="i_choose_heros" value="off" class="button_switch" onclick="calculate_addition(this.id)">I CHOOSE HEROES</button>
		</div>
</div>

</div>						
	
</div>

</div>

<div class="items_group clearfix price_buy">															
	<div id="price_dota">
	<input type="text" class="hide1" id="mmr_price" name="mmr_price" value="0"><span>€</span>
	</div>
	<label class="hide" for="mmr_price_base" style="font-size: 14px;line-height: 22px;color: #000;">mmr_price_base</label>
	<input class="hide" type="text" class="hide" id="mmr_price_base" name="mmr_price_base" value="2000">
	<label class="hide" for="mmr_price_additional" style="font-size: 14px;line-height: 22px;color: #000;">mmr_price_additional</label>
	<input class="hide" type="text" class="hide" id="mmr_price_additional" name="mmr_price_additional" value="0">
	<label class="hide" for="mmr_price_additional_rush" style="font-size: 14px;line-height: 22px;color: #000;">mmr_price_additional_rush</label>	
	<input type="text" class="hide" id="mmr_price_additional_rush" name="additional_rush" value="0">
	<label class="hide" for="mmr_price" style="font-size: 14px;line-height: 22px;color: #000;">mmr_points</label>
	<input type="text" class="hide" id="mmr_points" name="mmr_points" value="0">
	<label class="hide" for="mmr_price" style="font-size: 14px;line-height: 22px;color: #000;">estimated_days</label>
	<p style="text-align: center; width: 100%;">Estimated completion time: <span id="delivery"><input type="text" id="estimated_days" name="estimated_days" value="0"></span> day<span id="delivery_end">s</span></p>
	<div class="finishing column one-fifth add_to_cart_btn">
	<input type="submit" name="submit_prod"  class="single_add_to_cart_button button alt" value="Buy now">
	</div>

</div>			
				
			
</form>
	
</div>
</div>

	
<?php 
return ob_get_clean();
}
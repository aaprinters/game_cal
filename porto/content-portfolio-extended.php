<?php
global $porto_settings, $porto_layout;

$portfolio_layout = 'full';

$portfolio_info = get_post_meta($post->ID, 'portfolio_info', true);
$portfolio_name = empty( $porto_settings['portfolio-singular-name'] ) ? __( 'Portfolio', 'porto' ) : $porto_settings['portfolio-singular-name'];
$share = porto_get_meta_value('portfolio_share');

$post_class = array();
$post_class[] = 'portfolio-' . $portfolio_layout;
if ($porto_settings['post-title-style'] == 'without-icon')
    $post_class[] = 'post-title-simple';
?>

<article <?php post_class($post_class); ?>>

    <?php if ($porto_settings['portfolio-page-nav']) : ?>
    <div class="portfolio-title<?php echo ($porto_layout === 'widewidth' ? ' container m-t-lg' : '') ?>">
        <div class="row">

            <div class="portfolio-nav-all col-lg-1">
                <a title="<?php esc_html_e('Back to list', 'porto') ?>" data-tooltip href="<?php echo get_post_type_archive_link( 'portfolio' ) ?>"><i class="fa fa-th"></i></a>
            </div>
            <div class="col-lg-10 text-center">
                <h2 class="entry-title shorter"><?php the_title(); ?></h2>
            </div>
            <div class="portfolio-nav col-lg-1">
                <?php previous_post_link('%link', '<div data-tooltip title="'.esc_html__('Previous', 'porto').'" class="portfolio-nav-prev"><i class="fa"></i></div>'); ?>
                <?php next_post_link('%link', '<div data-tooltip title="'.esc_html__('Next', 'porto').'" class="portfolio-nav-next"><i class="fa"></i></div>'); ?>
            </div>
        </div>
    </div>
    <div class="m-t-xl"></div>
    <?php endif; ?>

    <?php porto_render_rich_snippets( false ); ?>

    <?php
    // Portfolio Slideshow
    $slideshow_type = get_post_meta($post->ID, 'slideshow_type', true);

    if (!$slideshow_type)
        $slideshow_type = 'images';

    $show_external_link = $porto_settings['portfolio-external-link'];

    $options = array();
    $options['themeConfig'] = true;
    if ($porto_layout === 'widewidth' && is_singular('portfolio')) {
        $options['dots'] = 0;
        $options['nav'] = 1;
        $options['stagePadding'] = 0;
    }
    $options = json_encode($options);

    if ($slideshow_type != 'none') : ?>
        <?php if ($slideshow_type == 'images') :
            $featured_images = porto_get_featured_images();
            $image_count = count($featured_images);

            if ($image_count) :
            ?>
            <div class="portfolio-image<?php if ($image_count == 1) echo ' single'; ?><?php if ($porto_layout === 'widewidth' && is_singular('portfolio')) echo ' wide' ?>">
                <div class="portfolio-slideshow porto-carousel owl-carousel<?php if ($porto_layout === 'widewidth' && is_singular('portfolio')) echo ' big-nav' ?>" data-plugin-options="<?php echo esc_attr($options) ?>">
                    <?php
                    foreach ($featured_images as $featured_image) {
                        $attachment = porto_get_attachment($featured_image['attachment_id']);
                        if ($attachment) {
                            ?>
                            <div>
                                <div class="img-thumbnail<?php if ($porto_layout === 'widewidth') echo ' img-thumbnail-no-borders' ?>">
                                    <img class="owl-lazy img-responsive" width="<?php echo $attachment['width'] ?>" height="<?php echo $attachment['height'] ?>" data-src="<?php echo $attachment['src'] ?>" alt="<?php echo $attachment['alt'] ?>" />
                                    <?php if ($porto_settings['portfolio-zoom']) : ?>
                                        <span class="zoom" data-src="<?php echo $attachment['src']; ?>" data-title="<?php echo $attachment['caption']; ?>"><i class="fa fa-search"></i></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <?php
            endif;
        endif;
        ?>

        <?php
        if ($slideshow_type == 'video') {
            $video_code = get_post_meta($post->ID, 'video_code', true);
            if ($video_code) {
                ?>
                <div class="portfolio-image single">
                    <div class="img-thumbnail fit-video<?php if ($porto_layout === 'widewidth') echo ' img-thumbnail-no-borders' ?>">
                        <?php echo do_shortcode($video_code) ?>
                    </div>
                </div>
            <?php
            }
        }
    endif;
    ?>

    <div class="post-content">

        <?php
        the_content();
        wp_link_pages( array(
            'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'porto' ) . '</span>',
            'after'       => '</div>',
            'link_before' => '<span>',
            'link_after'  => '</span>',
            'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'porto' ) . ' </span>%',
            'separator'   => '<span class="screen-reader-text">, </span>',
        ) );
        ?>

    </div>

    <div class="m-t-lg<?php echo ($porto_layout === 'widewidth' ? ' container' : '') ?>">
        <hr class="tall mt-0">

        <div class="portfolio-info pt-none">
            <ul>
                <?php if (in_array('like', $porto_settings['portfolio-metas'])) : ?>
                    <li>
                        <?php echo porto_portfolio_like() ?>
                    </li>
                <?php endif;
                if (in_array('date', $porto_settings['portfolio-metas'])) : ?>
                    <li>
                        <i class="fa fa-calendar"></i> <?php echo get_the_date() ?>
                    </li>
                <?php endif;
                $cat_list = get_the_term_list($post->ID, 'portfolio_cat', '', ', ', '');
                if (in_array('cats', $porto_settings['portfolio-metas']) && $cat_list) : ?>
                    <li>
                        <i class="fa fa-tags"></i> <?php echo $cat_list ?>
                    </li>
                <?php endif; ?>
                <?php
                if (function_exists('Post_Views_Counter') && Post_Views_Counter()->options['display']['position'] == 'manual' && in_array( 'portfolio', (array) Post_Views_Counter()->options['general']['post_types_count'] )) {
                    $post_count = do_shortcode('[post-views]');
                    if ($post_count) : ?>
                        <li>
                            <?php echo $post_count ?>
                        </li>
                    <?php endif;
                }
                ?>
            </ul>
        </div>

        <div class="row">
            <div class="<?php echo $porto_layout == 'wide-both-sidebar' || $porto_layout == 'both-sidebar' ? 'col-md-12' : 'col-md-7'; ?> mt-4 mb-4">
                <?php if ($portfolio_info) : ?>
                    <h4 class="m-t-sm"><?php esc_html_e('More Information', 'porto') ?></h4>
                    <div class="m-b-lg">
                        <?php echo do_shortcode(wpautop($portfolio_info)) ?>
                    </div>
                <?php endif; ?>

                <?php if ($porto_settings['share-enable'] && 'no' !== $share && ('yes' === $share || ('yes' !== $share && $porto_settings['portfolio-share']))) : ?>
                    <hr class="tall">
                    <div class="share-links-block">
                        <h5><?php esc_html_e( 'Share', 'porto' ) ?></h5>
                        <?php get_template_part('share') ?>
                    </div>
                <?php endif; ?>

            </div>
            <div class="<?php echo $porto_layout == 'wide-both-sidebar' || $porto_layout == 'both-sidebar' ? 'col-md-12' : 'col-md-5'; ?> mt-4">
                <?php porto_get_template_part( 'views/portfolios/meta', null, array(
                    'title_tag' => 'h4',
                    'title_class' => 'm-t-sm'
                ) ) ?>
            </div>
        </div>

        <?php if ($porto_settings['portfolio-author']) : ?>
            <div class="post-gap"></div>
            <div class="post-block post-author clearfix">
                <?php if ($porto_settings['post-title-style'] == 'without-icon') : ?>
                    <h4><?php esc_html_e('Author', 'porto') ?></h4>
                <?php else : ?>
                    <h3><i class="fa fa-user"></i><?php esc_html_e('Author', 'porto') ?></h3>
                <?php endif; ?>
                <div class="img-thumbnail">
                    <?php echo get_avatar(get_the_author_meta('email'), '80'); ?>
                </div>
                <p><strong class="name"><?php the_author_posts_link(); ?></strong></p>
                <p><?php the_author_meta("description"); ?></p>
            </div>
        <?php endif; ?>

        <?php if ($porto_settings['portfolio-comments']) : ?>
            <div class="post-gap"></div>
            <?php
            wp_reset_postdata();
            comments_template();
            ?>
        <?php endif; ?>

    </div>

</article>
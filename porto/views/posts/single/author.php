<?php
    global $porto_settings;
?>

<?php if ($porto_settings['post-author']) : ?>
    <div class="post-block post-author clearfix">
        <?php if ($porto_settings['post-title-style'] == 'without-icon') : ?>
            <h3><?php _e('Author', 'porto') ?></h3>
        <?php else : ?>
            <h3><i class="fa fa-user"></i><?php _e('Author', 'porto') ?></h3>
        <?php endif; ?>
        <div class="img-thumbnail">
            <?php echo get_avatar(get_the_author_meta('email'), '80'); ?>
        </div>
        <p><strong class="name"><?php the_author_posts_link(); ?></strong></p>
        <p><?php the_author_meta("description"); ?></p>
    </div>
<?php endif; ?>